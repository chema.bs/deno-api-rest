import { Response, Request, bcrypt, makeJwt, Cookies } from '../utils/deps.ts';
import { key, payload, header } from "../middlewares/jwt.ts";
import UserModel from "../models/user.model.ts";

export default {
  signUpUser: async (
    {response, request, cookies }: {
      request: any;
      response: Response;
      cookies: Cookies;
    }
  ) => {
    if (request.auth){
      cookies.set("message", `You have already entered Cybertron`);
      response.redirect("/")
    }
    else{
      const body = await request.body().value;
      const user = await UserModel.getCredentialsUser(
        { name: String(body.name) },
      );
      if(user){
          cookies.set("message", "User already exists");
          response.redirect("/");
      }
      else{
          const user = await UserModel.add({
              username: body.name,
              password: await bcrypt.hash(body.password),
              admin: false,
          });
          cookies.set("message", `Successfully registered as a prime: ${body.name}. Enter Cybertron to continue..`);
          response.redirect("/")
      }
    }
  },

  loginUser: async (
    {response, request, cookies }: {
      request: any;
      response: Response;
      cookies: Cookies;
    }
  ) => {
    const body = await request.body().value;

    if (Object.keys(body).length === 0) {
      response.status = 400;
      response.body = {
        success: false,
        message: "No data provided",
      };
      return
    }
    if (body.name === '' || body.password === '') {
      response.status = 400;
      response.body = {
        success: false,
        message: "User and Pass is Required",
      };
      return
    }

    try {
      const user = await UserModel.getCredentialsUser(
        { name: String(body.name) },
      );

      if (Object.keys(user).length === 0){
        console.log("No existe el usuario");
      }
      else{
        if(await bcrypt.compare(body.password, user[0].password)){
          //payload.name = body.name;
          const make = await makeJwt({ header, payload, key });
          cookies.set("token", make);
          response.status = 200;
          response.body = {
            success: true,
            message: "User Logged",
          };
          response.redirect("/");
        }
        else{
          response.status = 400;
            response.body = {
              success: false,
              message: "User and Pass is Required",
            };
            response.redirect("/login");
        }
        return
      }
    } catch (error) {
      console.log(`Este es un error: ${error}`);
    }
  },
  logoutUser: async () => {},
}
