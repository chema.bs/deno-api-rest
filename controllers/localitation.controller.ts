import {Request, Response } from '../utils/deps.ts';
import { Localitation } from '../interfaces/localitationInterface.ts';

const localitations: Localitation = [{
    id: "1",
    name: "Localitation",
    description: "Description"
}];

export const getLocalitations = ({ response }: { response: Response }) => {
    response.body = {
        message: "Sucessful Query Localitations",
        localitations,
    };
};
