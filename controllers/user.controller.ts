import {Request, Response, bcrypt, v4, Context } from '../utils/deps.ts';
import { User } from '../interfaces/user.interface.ts';
//model
import UserModel from "../models/user.model.ts";

export default {
  /**
   * @description Get all users
   * @route GET /users
   */
  getAllUsers: async (
    { response }: { response: Response },
  ) => {
    try {
      const data = await UserModel.getAll();
      response.status = 200;
      response.body = {
        success: true,
        data,
      };
    } catch (error) {
      response.status = 400;
      response.body = {
        success: false,
        message: `Error: ${error}`,
      };
    }
  },
  /**
   * @description Add a new user
   * @route POST /users
   */
  createUser: async (
    { request, response }: { request: Request; response: Response },
  ) => {
    const body = await request.body().value;
    const hash = await bcrypt.hash("Erjm0918_pi");
    //const password = await bcrypt.hash((body).password);
    const password = await UserModel.hashPassword((body).password);
    // Generate a v4 uuid
    const pUUID = v4.generate();

    if (Object.keys(body).length === 0) {
      response.status = 400;
      response.body = {
        success: false,
        message: "No data provided",
      };
      return;
    }

    try {
      UserModel.add(
        { public_id: pUUID, name: (body).name, password: password, admin: false },
      );
      response.body = {
        success: true,
        message: "The record was added successfully",
      };
    } catch (error) {
      response.status = 400;
      response.body = {
        success: false,
        message: `Error: ${error}`,
      };
    }
  },
  /**
   * @description Get user by id
   * @route GET users/:id
   */
  getUserById: async (
    { params, response }: { params: { id: string }; response: Response },
  ) => {
    try {
      const isAvailable = await UserModel.doesExistById(
        { id: Number(params.id) },
      );

      if (!isAvailable) {
        response.status = 404;
        response.body = {
          success: false,
          message: "No user found",
        };
        return;
      }

      const user = await UserModel.getById({ id: Number(params.id) });
      response.status = 200;
      response.body = {
        success: true,
        data: user,
      };
    } catch (error) {
      response.status = 400;
      response.body = {
        success: false,
        message: `Error: ${error}`,
      };
    }
  },
  /**
   * @description Update user by id
   * @route PUT users/:id
   */
  updateUserById: async (
    { params, request, response }: {
      params: { id: string };
      request: Request;
      response: Response;
    },
  ) => {
    try {
      const isAvailable = await UserModel.doesExistById(
        { id: Number(params.id) },
      );
      if (!isAvailable) {
        response.status = 400;
        response.body = {
          success: false,
          message: "No user found",
        };
        return;
      }
      //if user found then update user
      const body = await request.body().value;
      const updatedRows = await UserModel.updateById({
        id: Number(params.id),
        ...body,
      });
      response.status = 200;
      response.body = {
        success: true,
        message: `Successfully updated ${updatedRows} row(s)`,
      };
    } catch (error) {
      response.status = 400;
      response.body = {
        success: false,
        message: `Error ${error}`,
      };
    }
  },
  /**
   * @description Delete user by id
   * @route DELETE users/:id
   */
  deleteUserById: async (
    { params, response }: { params: { id: string }; response: Response },
  ) => {
    try {
      const deletedRows = await UserModel.deleteById({
        id: Number(params.id),
      });
      response.status = 200;
      response.body = {
        success: true,
        message: `Successfully deleted ${deletedRows} row(s)`,
      };
    } catch (error) {
      response.status = 400;
      response.body = {
        success: false,
        message: `Error: ${error}`,
      };
    }
  },

  // Auht User

  /**
   * @description Get user by user and pass
   * @route GET login/:user/:pass
   */
  getUserPass: async (
    ctx: Context
  ) => {
    const params = await ctx.request.body();
    console.log(params);
    try {
      //const {userExist} = await UserModel.getUserPass({ user:String(params.user), pass:String(params.pass) });
      //console.log(userExist);
      // if (!userExist) {
      //   response.status = 404;
      //   response.body = {
      //     success: false,
      //     message: "No user found",
      //   };
      //   return;
      // }

      // const user = await UserModel.getById({ id: Number(params.id) });
      // response.status = 200;
      // response.body = {
      //   success: true,
      //   data: user,
      // };
    } catch (error) {
      // response.status = 400;
      // response.body = {
      //   success: false,
      //   message: `Error: ${error}`,
      // };
    }
  },
};
