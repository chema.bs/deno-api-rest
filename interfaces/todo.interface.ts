/**
 * Todo
 */
export default interface Todo {
  id?: number;
  todo?: string;
  is_completed?: boolean;
}
