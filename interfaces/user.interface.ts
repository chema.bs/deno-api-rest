interface User {
    id: string
    public_id: string
    name: string
    password: string
    admin: boolean
}
