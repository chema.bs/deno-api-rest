import { setExpiration } from "../utils/deps.ts";

export const key = "meeli";

export const payload = {
  exp: setExpiration(new Date().getTime() + 60000*60),
  name: "",
}

export const header = {
  typ: "JWT",
  alg: "HS256" as any,
}
