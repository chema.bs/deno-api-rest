import client from "../db/client.ts";
// config
import { TABLE } from "../db/config.ts";
// Interface
import Todo from "../interfaces/todo.interface.ts";

export default {
  /**
   * Takes in the id params & checks if the todo item exists
   * in the database
   * @param id
   * @returns boolean to tell if an entry of todo exits in table
   */
  doesExistById: async ({ id }: Todo) => {
    const [result] = await client.query(
      `SELECT COUNT(*) count FROM ${TABLE.TODO} WHERE id = ? LIMIT 1`,
      [id],
    );
    return result.count > 0;
  },
  /**
   * Will return all the entries in the todo column
   * @returns array of todos
   */
  getAll: async () => {
    return await client.query(`SELECT * FROM ${TABLE.TODO}`);
  },
  /**
   * Takes in the id params & returns the todo item found
   * against it.
   * @param id
   * @returns object of todo item
   */
  getById: async ({ id }: Todo) => {
    return await client.query(
      `SELECT * FROM ${TABLE.TODO} where id = ?`,
      [id],
    );
  },
  /**
   * Adds a new todo item to todo table
   * @param todo
   * @param is_completed
   */
  add: async ({ todo, is_completed }: Todo) => {
    return await client.query(
      `INSERT INTO ${TABLE.TODO} (todo, is_completed) values(?, ?)`,
      [
        todo,
        is_completed,
      ],
    );
  },
  /**
   * Updates the content of a single todo item
   * @param id
   * @param todo
   * @param is_completed
   * @returns integer (count of effect rows)
   */
  updateById: async ({ id, todo, is_completed }: Todo) => {
    const result = await client.query(
      `UPDATE ${TABLE.TODO} SET todo=?, is_completed=? WHERE id=?`,
      [
        todo,
        is_completed,
        id,
      ],
    );
    // return count of rows updated
    return result.affectedRows;
  },
  /**
   * Deletes a todo by ID
   * @param id
   * @returns integer (count of effect rows)
   */
  deleteById: async ({ id }: Todo) => {
    const result = await client.query(
      `DELETE FROM ${TABLE.TODO} WHERE id=?`,
      [id],
    );
    // return count of rows deleted
    return result.affectedRows;
  },
};
