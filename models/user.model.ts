import client from "../db/client.ts";
import { Response, Request, bcrypt } from '../utils/deps.ts';
// config
import { TABLE } from "../db/config.ts";
// Interface
import User from "../interfaces/user.interface.ts";

export default {
  /**
   * Takes in the id params & checks if the user item exists
   * in the database
   * @param id
   * @returns boolean to tell if an entry of user exits in table
   */
  doesExistById: async ({ id }: User) => {
    const [result] = await client.query(
      `SELECT COUNT(*) count FROM ${TABLE.USER} WHERE id = ? LIMIT 1`,
      [id],
    );
    return result.count > 0;
  },
  /**
   * Will return all the entries in the user column
   * @returns array of users
   */
  getAll: async () => {
    return await client.query(`SELECT * FROM ${TABLE.USER}`);
  },
    /**
   * Takes in the user & pass params & returns the user item found
   * against it.
   * @param user, pass
   * @returns object of user item
   */
  getCredentialsUser: async ({ name }: User) => {
    return await client.query(`SELECT * FROM ${TABLE.USER} WHERE name = ?`,
    [name],
    );
  },

  /**
   * Takes in the id params & returns the user item found
   * against it.
   * @param id
   * @returns object of user item
   */
  getById: async ({ id }: User) => {
    return await client.query(
      `SELECT public_id, name, admin FROM ${TABLE.USER} where id = ?`,
      [id],
    );
  },
  /**
   * Adds a new user item to user table
   * @param public_id
   * @param name
   * @param password
   * @param admin
   */
  add: async ({ public_id, name, password, admin }: User) => {
    return await client.query(
      `INSERT INTO ${TABLE.USER} (public_id, name, password, admin) values(?, ?, ?, ?)`,
      [
        public_id,
        name,
        password,
        admin,
      ],
    );
  },
  /**
   * Updates the content of a single user item
   * @param id
   * @param name
   * @param admin
   * @returns integer (count of effect rows)
   */
  updateById: async ({ id, name, admin }: User) => {
    const result = await client.query(
      `UPDATE ${TABLE.USER} SET name=?, admin=? WHERE id=?`,
      [
        name,
        admin,
        id,
      ],
    );
    // return count of rows updated
    return result.affectedRows;
  },
  /**
   * Deletes a user by ID
   * @param id
   * @returns integer (count of effect rows)
   */
  deleteById: async ({ id }: User) => {
    const result = await client.query(
      `DELETE FROM ${TABLE.USER} WHERE id=?`,
      [id],
    );
    // return count of rows deleted
    return result.affectedRows;
  },

  hashPassword: async (password: string) => {
    const salt = await bcrypt.genSalt(8);
    return bcrypt.hash(password, salt);
  }
};
