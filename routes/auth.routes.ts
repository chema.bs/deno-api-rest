import { Router } from "../utils/deps.ts";

const auth = new Router();
// controller
import authController from "../controllers/auth.controller.ts";

auth
  .post("/signup", authController.signUpUser)
  .post("/login", authController.loginUser)
  .post("/logout", authController.logoutUser);

export default auth;
