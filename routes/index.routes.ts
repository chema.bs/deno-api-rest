import auth from "../routes/auth.routes.ts";
import user from "../routes/users.routes.ts";
import localitation from "../routes/localitations.routes.ts";
import todo from "../routes/todos.routes.ts";

export default {
  auth,
  user,
  localitation,
  todo,
}
