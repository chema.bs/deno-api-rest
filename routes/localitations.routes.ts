import { Router, Response } from "../utils/deps.ts";
import * as localitationCtrl from '../controllers/localitation.controller.ts';

const localitation = new Router();

localitation.get('/localitations', localitationCtrl.getLocalitations);

export default localitation;
