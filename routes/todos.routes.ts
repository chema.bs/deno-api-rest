import { Router } from "../utils/deps.ts";

const todo = new Router();
// controller
import todoController from "../controllers/todo.controller.ts";

todo
  .get("/todos", todoController.getAllTodos)
  .post("/todos", todoController.createTodo)
  .get("/todos/:id", todoController.getTodoById)
  .put("/todos/:id", todoController.updateTodoById)
  .delete("/todos/:id", todoController.deleteTodoById);

export default todo;
