import { Router } from "../utils/deps.ts";

const user = new Router();
// controller
import userController from "../controllers/user.controller.ts";

user
  .get("/users", userController.getAllUsers)
  .post("/users", userController.createUser)
  .get("/users/:id", userController.getUserById)
  .put("/users/:id", userController.updateUserById)
  .delete("/users/:id", userController.deleteUserById);

export default user;
