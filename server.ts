import { Application, green, yellow, red } from "./utils/deps.ts";
import { validate } from "./utils/validate.ts";
import logger from "./utils/logger.ts";
import routes from "./routes/index.routes.ts";

const app = new Application();
const port: number = 3001;

app.addEventListener("listen", ({ secure, hostname, port }) => {
  const protocol = secure ? "https://" : "http://";
  const url = `${protocol}${hostname ?? "localhost"}:${red(port.toString())}`;
  console.log(`${yellow("Listening on:")} ${green(url)}`);
});
// order of execution is important;
// app.use(validate);

app.use(logger.logger);
app.use(logger.responseTime);

app.use(routes.auth.routes());
app.use(routes.auth.allowedMethods());

app.use(routes.user.routes());
app.use(routes.user.allowedMethods());

app.use(routes.localitation.routes());
app.use(routes.localitation.allowedMethods());

app.use(routes.todo.routes());
app.use(routes.todo.allowedMethods());

await app.listen({ port: port });
